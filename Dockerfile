
FROM python:3-slim
COPY argon2-benchmark.sh /
RUN set -e ; \
    python3 -m venv /venv; \
    /venv/bin/pip install argon2-cffi; \
    /venv/bin/pip uninstall --yes setuptools pkg_resources pip; \
    find /venv -type d -name __pycache__ -prune -exec rm -rf {} \; ;\
    true;
CMD [ "/argon2-benchmark.sh" ]

